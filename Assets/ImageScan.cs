using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;

public class ImageScan : MonoBehaviour
{
    private ARTrackedImageManager _arTrackedImageManager;
    public FlexibleColorPicker fcp;
    public static ImageScan instance;
    private void Awake()
    {
        _arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        instance = this;
    }
    public void OnEnable()
    {
        _arTrackedImageManager.trackedImagesChanged += OnImageChanged;

    }
    public void OnDisable()
    {
        _arTrackedImageManager.trackedImagesChanged -= OnImageChanged;

    }

    public void OnImageChanged(ARTrackedImagesChangedEventArgs args)
    {
        foreach (var trakedImage in args.added)
        {
            Debug.Log(trakedImage.name);
        }
    }
    public void QuitColor()
    {
        fcp.gameObject.SetActive(false);
    }

}