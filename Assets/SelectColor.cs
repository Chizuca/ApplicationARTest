using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectColor : MonoBehaviour
{
    void OnMouseDown()
    {
        ImageScan.instance.fcp.gameObject.SetActive(true);
    }
    void Update()
    {
        gameObject.GetComponent<Renderer>().material.color = ImageScan.instance.fcp.color;
    }
}
